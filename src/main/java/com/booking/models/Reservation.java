package com.booking.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation {
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;
    //   workStage (In Process, Finish, Canceled)

    public Reservation(String reservationId, Customer customer, Employee employee, List<Service> services,
            String workstage) {
        this.reservationId = reservationId;
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.reservationPrice = calculateReservationPrice();
        this.workstage = workstage;
    };

    private double calculateReservationPrice(){
        String customerMembership = customer.getMember().getMembershipName();
        String[] customerMembershipList = {"none", "Silver", "Gold"};
        double[] discountList = {0.0, 0.05, 0.10};

        Double serviceTotalPrice = services.stream()
                                        .mapToDouble(Service::getPrice)
                                        .sum();
        
        for (int i = 0; i < customerMembershipList.length; i++) {
            if (customerMembership.equals(customerMembershipList[i])) {
                return (double) (serviceTotalPrice - (serviceTotalPrice * discountList[i]));
            }
        }

        return 0;
    }
}

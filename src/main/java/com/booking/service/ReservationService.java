package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ReservationService {

    public static void createReservation(List<Reservation> reservationList, List<Person> personList, List<Service> serviceList) {
        int reservationId = reservationList.size() + 1;
        String reservationUniqueId = String.format("Reserv-%02d", reservationId);

        PrintService.showAllCustomer(personList);
        String customerId = ValidationService.validateCustomerId("Cust-[0-9]{2}");
        Customer selectedCustomer = getCustomerByCustomerId(personList, customerId);

        PrintService.showAllEmployee(personList);
        String employeeId = ValidationService.validateEmployeeId("Emp-[0-9]{2}");
        Employee selectedEmployee = getEmployeeByEmployeeId(personList, employeeId);

        System.out.println("Service List : ");
        System.out.println(PrintService.printServices(serviceList));
        
        boolean selectingService = true;
        List<Service> selectedServices = new ArrayList<>();
        do{
            String serviceId = ValidationService.validateServiceId("0|Serv-[0-9]{2}");
            if (serviceId.equals("0")) { selectingService = false; break; }           
            else {
                Service selectService = serviceList.stream()
                                    .filter(service -> service.getServiceId().equals(serviceId))
                                    .findFirst()
                                    .orElse(null);
                selectedServices.add(selectService);
            }

        }while(selectingService);

        Double totalPrice = selectedServices.stream()
                            .mapToDouble(Service::getPrice)
                            .sum();
                            
        if (selectedCustomer.getWallet() < totalPrice) {
            System.out.println("Saldo customer tidak mencukupi!");
            return;
        } else {
            if (selectedServices.isEmpty()) { System.out.println("Service tidak boleh kosong!"); return; }
            Reservation newReservation = new Reservation(reservationUniqueId, selectedCustomer, selectedEmployee, selectedServices, "In process");
            reservationList.add(newReservation);
        }

    }

    public static Customer getCustomerByCustomerId(List<Person> personList, String customerId){
        Customer selectedCustomer = personList.stream()
                                        .filter(customer -> customer instanceof Customer)
                                        .map(customer -> (Customer) customer)
                                        .filter(customer -> customer.getId().equals(customerId))
                                        .findFirst()
                                        .orElse(null);

        System.out.println("-------------------------------------");
        System.out.println("Customer Id : " + selectedCustomer.getId());
        System.out.println("Customer Name : " + selectedCustomer.getName());
        System.out.println("Customer Membership : " + selectedCustomer.getMember().getMembershipName());

        return selectedCustomer;
    }

    public static Employee getEmployeeByEmployeeId(List<Person> personList, String employeeId){

        Employee selectedEmployee = personList.stream()
                                        .filter(employee -> employee instanceof Employee)
                                        .map(employee -> (Employee) employee)
                                        .filter(employee -> employee.getId().equals(employeeId))
                                        .findFirst()
                                        .orElseThrow(() -> new NoSuchElementException("Employee yang dicari tidak tersedia."));

        System.out.println("-------------------------------------");
        System.out.println("Employee Id : " + selectedEmployee.getId());
        System.out.println("Employee Name : " + selectedEmployee.getName());
        System.out.println("Employee Membership : " + selectedEmployee.getExperience());

        return selectedEmployee;
    }

    public static void editReservationWorkstage(List<Reservation> reservationsList){
        PrintService.showHistoryReservation(reservationsList);
        String reservationId = ValidationService.validateReservationId("Reserv-[0-9]{2}");
        System.out.println("Masukkan Workstage baru (Canceled|Finish) : ");
        String workstage = ValidationService.validateInput("Canceled|Finish");

        Reservation selectedReservation = reservationsList.stream()
                                        .filter(reservation -> reservation.getReservationId().equalsIgnoreCase(reservationId))
                                        .findFirst()
                                        .orElse(null);
        selectedReservation.setWorkstage(workstage);
    }

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}

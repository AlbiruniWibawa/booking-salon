package com.booking.service;

import java.util.List;
import java.util.stream.Collectors;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public static String printServices(List<Service> serviceList){
        String result = "";
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result += "(" + service.getServiceId() + ") " + service.getServiceName() + ", ";
        }
        return result;
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public static void showRecentReservation(List<Reservation> reservationList){
        int num = 1;
        System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+========================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting") || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                num++, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
            }
            System.out.println("+========================================================================================+");
        }
    }

    public static void showAllCustomer(List<Person> personList){
        List<Customer> customerList = personList.stream()
                                    .filter(person -> person instanceof Customer)
                                    .map(customer -> (Customer) customer)
                                    .collect(Collectors.toList());
        
        customerList.stream().forEach(customer -> {
                System.out.println("-------------------------------------");
                System.out.println("Customer ID : " + customer.getId());    
                System.out.println("Customer Name : " + customer.getName());
                System.out.println("Customer Membership : " + customer.getMember().getMembershipName());
                System.out.println("Customer Wallet : " + customer.getWallet());
            });
    }

    public static void showAllEmployee(List<Person> personList){
        List<Employee> employeeList = personList.stream()
                                    .filter(person -> person instanceof Employee)
                                    .map(employee -> (Employee) employee)
                                    .collect(Collectors.toList());
        
        employeeList.stream().forEach(employee -> {
                System.out.println("-------------------------------------");
                System.out.println("Employee ID : " + employee.getId());    
                System.out.println("Employee Name : " + employee.getName());
                System.out.println("Employee Experience : " + employee.getExperience() + " Tahun");
            });
    }

    public static void showHistoryReservation(List<Reservation> reservationList){
        int num = 1;
        double totalProfit = 0.0;
        System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+========================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equals("Finish")) {
                totalProfit += reservation.getReservationPrice();
            }
            System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                num++, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
            System.out.println("+========================================================================================+");
            System.out.println("Total Profit : " + totalProfit);
        }
    }

}

package com.booking.service;

import java.util.Scanner;

public class ValidationService {
    public static Scanner input = new Scanner(System.in);

    public static String validateInput(String regex){
        String result;
        do{
            result = input.nextLine();
            if(!result.matches(regex)){
                System.out.println("Pilihan tidak tersedia. Coba lagi!");
                System.out.println();
            }
        } while(!result.matches(regex));
        
        return result;
    }

    public static String validateCustomerId(String regex){
        String result;
        do{
            System.out.print("Enter customer id : ");
            result = input.nextLine();
            if(!result.matches(regex)){
                System.out.println("Customer yang dicari tidak tersedia. Coba lagi!");
                System.out.println();
            }
        } while(!result.matches(regex));
        
        return result;
    }

    public static String validateEmployeeId(String regex){
        String result;
        do{
            System.out.print("Enter employee id : ");
            result = input.nextLine();
            if(!result.matches(regex)){
                System.out.println("Employee yang dicari tidak tersedia. Coba lagi!");
                System.out.println();
            }
        } while(!result.matches(regex));
        
        return result;
    }

    public static String validateServiceId(String regex){
        String result;
        do{
            System.out.print("Enter Service id (input '0' to finish selecting) : ");
            result = input.nextLine();
            if(!result.matches(regex)){
                System.out.println("Service yang dicari tidak tersedia. Coba lagi!");
                System.out.println();
            }
        } while(!result.matches(regex));
        
        return result;
    }

    public static String validateReservationId(String regex){
        String result;
        do{
            System.out.print("Enter Reservation id : ");
            result = input.nextLine();
            if(!result.matches(regex)){
                System.out.println("Reservation yang dicari tidak tersedia. Coba lagi!");
                System.out.println();
            }
        } while(!result.matches(regex));
        
        return result;
    }
}
